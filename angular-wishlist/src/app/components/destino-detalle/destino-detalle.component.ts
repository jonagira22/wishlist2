import { Component, OnInit, InjectionToken, Inject, Injectable } from '@angular/core';
import {DestinosApiClient} from  './../../models/destinos-api-client.model';
import {DestinoViaje} from  './../../models/destino-viaje.model';
import {ActivatedRoute} from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.module';
import { HttpClient } from '@angular/common/http';


class DestinosApiClientViejo {
  getById(id: string): DestinoViaje {
    console.log('Usando DestinosApiClientViejo...');
    return null;
  }
}

interface AppConfig {
  apiEndpoint: string;
}

const APP_CONFIG_VALUE: AppConfig = {
  apiEndpoint: 'mi_api.com'
};

const APP_CONFIG = new InjectionToken<AppConfig>('app.config');

@Injectable()
class DestinosApiClientDecorated extends DestinosApiClient {
  constructor(
    store: Store<AppState>,
    @Inject(APP_CONFIG) config: AppConfig,
    http: HttpClient
  ) {
    super(store, config, http);
  }

  getById(id: string): DestinoViaje {
    console.log('Usando DestinosApiDecorated...');
    // console.log('config: ' + this.config.apiEndpoint);
    return super.getById(id);
  }
}

@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: ['./destino-detalle.component.css'],
  providers: [
    { provide: APP_CONFIG, useValue: APP_CONFIG_VALUE },
    { provide: DestinosApiClient, useClass: DestinosApiClientDecorated },
    { provide: DestinosApiClientViejo, useExisting: DestinosApiClient }
  ],
  styles: [`
    mgl-map {
      height: 60vh;
      width: 125vw;
    }
  `]
})
export class DestinoDetalleComponent implements OnInit {
  destino: DestinoViaje;
  style = {
    sources: {
      world: {
        type: "geojson",
        data: "https://raw.githubusercontent.com/johan/world.geo.json/master/countries/COL.geo.json"
      }
    },
    version: 8,
    layers: [{
      "id": "countries",
      "type": "fill",
      "source": "world",
      "layout": {},
      "paint": {
        'fill-color': '#6F788A'
      }
    }]
  }

  constructor(private route: ActivatedRoute, private destinoApiClient: DestinosApiClient) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.destino = this.destinoApiClient.getById(id);
  }

}